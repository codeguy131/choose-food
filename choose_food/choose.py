"""Randomly select a food choice from user provided input"""

import random
from sys import argv


def choose():
    """Choose food"""

    args_list = list(argv[1:])
    res = args_list[random.randint(0, len(args_list) - 1)]
    print(res)


if __name__ == "__main__":
    choose()
